﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ButtonRollABall () {
		SceneManager.LoadScene("RollABall", LoadSceneMode.Single);
	}

	public void ButtonSpaceShooter () {
		SceneManager.LoadScene("SpaceShooter", LoadSceneMode.Single);
	}

	public void ButtonQuitGame () {
		Application.Quit();
	}

	public void ButtonMainMenu() {
		SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
	}

}
